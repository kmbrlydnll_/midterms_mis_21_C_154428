class ToDoList

def initialize()
 @pending_tasks = []
 @accomplished_tasks = []
end

#METHODS
def add_task(task_name)
    @pending_tasks << task_name
    puts "#{task_name} has been added to your task list!"
end

def view_pending_tasks
    number = 0
    puts "Pending tasks:"
    @pending_tasks.each do |a|
        number += 1
        puts "(#{number}) #{a}"
    end
end

def accomplish_task(index)
    puts "#{@pending_tasks[index]} has been marked as accomplished."
    @accomplished_tasks << @pending_tasks.delete_at(index)
end

def view_accomplished_tasks
    number = 0
    puts "Accomplished tasks:"
    @accomplished_tasks.each do |a|
        number += 1
        puts "(#{number}) #{a}"
    end
end

end