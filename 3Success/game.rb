require './person'

@listnames = []
@listnames << Person.new(name, success_points)

first_person = @listnames.first
second_person = @listnames.sample(1)


loop do
    puts "Who's likely to succeed? Press any key to exit."
    puts "1: #{first_person}                 2: #{second_person}" 
    choice = gets.chomp
    case choice
        when 1
            self.update_success_points(first_person, second_person)
            second_person = @listnames.sample(1)
        when 2
            self.update_success_points(second_person, first_person)
            first_person = @listnames.sample(1)
        else
            puts "Exiting...."
        end
    break if choice != 1 || choice != 2
end

@listnames.sort! {|x,y| y.success_points <=> x.success_points}
puts "Name:        Success Points:"
    @listnames.each do |a|
        if a.success_points > 0
        puts "#{a}       #{a.success_points}"
        end

end
      
