require 'date'
class Budget
attr_accessor :balance, :total_expense, :total_income
def initialize(balance)
    puts "Initial Balance: #{balance}"
    @balance = balance
    @total_expense = 0.00
    @total_income = 0.00
    @january_inc, @february_inc, @march_inc, @april_inc, @may_inc, @june_inc, @july_inc, @august_inc, @september_inc, @october_inc, @november_inc, @december_inc = 0.00
    @january_exp, @february_exp, @march_exp, @april_exp, @may_exp, @june_exp, @july_exp, @august_exp, @september_exp, @october_exp, @november_exp, @december_exp = 0.00
end

#Methods
def add_expense(item, amount, date)
    @balance = balance - amount.to_f
    b = Date.parse(date)
    c = date.split("-")
    if c[1] == '01'
        @january_exp += amount.to_f
	elsif c[1]=='02'
		@february_exp += amount.to_f
	elsif c[1]=='03'
		@march_exp += amount.to_f
    elsif c[1] == '04'
        @april_exp+=amount.to_f
	elsif c[1]=='05'
		@may_exp+=amount.to_f
	elsif c[1]=='06'
		@june_exp += amount.to_f 
    elsif c[1] == '07'
        @july_exp += amount.to_f
	elsif c[1]=='08'
		@august_exp += amount.to_f
	elsif c[1]=='09'
		@september_exp += amount.to_f
    elsif c[1] == '10'
        @october_exp += amount.to_f
	elsif c[1]=='11'
		@november_exp += amount.to_f
	elsif c[1]=='12'
		@december_exp += amount.to_f
	end
    puts "#{item}: An amount of #{amount} was deducted from the balance on #{b.strftime("%B %d, %Y")}"
end

def add_income(item, amount, date)
    @balance = @balance + amount.to_f
    b = Date.parse(date)
    c = date.split("-")
    if c[1] == '01'
        @january_inc += amount.to_f
	elsif c[1]=='02'
		@february_inc += amount.to_f
	elsif c[1]=='03'
		@march_inc += amount.to_f
    elsif c[1] == '04'
        @april_inc += amount.to_f
	elsif c[1]=='05'
		@may_inc += amount.to_f
	elsif c[1]=='06'
		@june_inc += amount.to_f 
    elsif c[1] == '07'
        @july_inc += amount.to_f
	elsif c[1]=='08'
		@august_inc += amount.to_f
	elsif c[1]=='09'
		@september_inc += amount.to_f
    elsif c[1] == '10'
        @october_inc += amount.to_f
	elsif c[1]=='11'
		@november_inc += amount.to_f
	elsif c[1]=='12'
		@december_inc += amount.to_f
	end
    puts "#{item}: An amount of #{amount} was added to the balance on #{b.strftime("%B %d, %Y")}"
end


def monthly_report(month,year)
    
    a = Date::MONTHNAMES[month]
    puts "====================================="
    puts "MONTHLY REPORT: #{a} #{year}"
    if month == 1
        puts "TOTAL EXPENSE: #{@january_exp}"
        puts "TOTAL INCOME #{@january_inc}"
    elsif month == 2
        puts "TOTAL EXPENSE: #{@february_exp}"
        puts "TOTAL INCOME #{@february_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@march_exp}"
        puts "TOTAL INCOME #{@march_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@april_exp}"
        puts "TOTAL INCOME #{@april_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@may_exp}"
        puts "TOTAL INCOME #{@may_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@june_exp}"
        puts "TOTAL INCOME #{@june_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@july_exp}"
        puts "TOTAL INCOME #{@july_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@august_exp}"
        puts "TOTAL INCOME #{@august_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@september_exp}"
        puts "TOTAL INCOME #{@september_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@october_exp}"
        puts "TOTAL INCOME #{@october_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@november_exp}"
        puts "TOTAL INCOME #{@november_inc}"
    elsif month == 1
        puts "TOTAL EXPENSE: #{@december_exp}"
        puts "TOTAL INCOME #{@december_inc}"
    end
    puts "====================================="   
end

def current
    puts "Balance: #{@balance}"
end

end
