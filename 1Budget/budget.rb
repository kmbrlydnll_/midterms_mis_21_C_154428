require 'date'

class Log
attr_accessor :month, :year, :amount
    def initialize(month, year, amount)
      @month = month
      @year = year
      @amount = amount
    end
end

class Budget
attr_accessor :balance, :month, :year, :amount
def initialize(balance)
    puts "Initial Balance: #{balance}"
    @balance = balance
    @log_expense = []
    @log_income = []
end

#Methods
def add_expense(item, amount, date)
    @balance = balance - amount.to_f
    b = Date.parse(date)
    puts "#{item}: An amount of #{amount} was deducted from the balance on #{b.strftime("%B %d, %Y")}"
    @log_expense << Log.new(b.strftime("%m"), b.strftime("%Y"), amount)
end

def add_income(item, amount, date)
    @balance = @balance + amount.to_f
    b = Date.parse(date)
    puts "#{item}: An amount of #{amount} was added to the balance on #{b.strftime("%B %d, %Y")}"
    @log_income << Log.new(b.strftime('%m'), b.strftime("%Y"), amount)
end


def monthly_report(month,year)
    total_expense = 0.00
    total_income = 0.00
    a = Date::MONTHNAMES[month]
    puts "====================================="
    puts "MONTHLY REPORT: #{a} #{year}"
    @log_expense.each do |b|
        if b.month == month && b.year == year
            total_expense = total_expense + b.amount.to_f
        end
    end
    puts "TOTAL EXPENSE: #{total_expense}"

    @log_income.each do |c|
        if c.month == month && c.year ==year
            total_income = total_income + c.amount.to_f
        end
    end
    puts "TOTAL INCOME: #{total_income}"
    puts "====================================="   
end

def current
    puts "Balance: #{@balance}"
end

end
